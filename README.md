## CKEditor 5 - Tags ♥️
This module integrates directly into CKEditor 5 and allows you to integrate rich semantic tags inside your text contents.

### What could I use it for?
Let's imagine you have a component into a page that should display data from a remote source:
This plugin will let you inject dynamic data into a totally static and "unaware" component, effectively decoupling the data-logic from its representation on its context.

### Where can I use tags?
Everywhere rich text can be placed! The modules comes with a handy JS library for easy data substitution with spinners.
Tags can also be styled as you want ❤️.

### Installation
- Install and Enable CKEditor 5
- Create a rich text format:
- /admin/config/content/formats
- Install this module
- Drag & drop the color and dynamic tags widget into the active toolbar

### How do I use it?

- After the installations steps, contribute any number of dynamicTags through the CKEditor, assigning an ID and a Label
- In the page, all the tags will display their label by default, until their value is requested to change.
- In the window object the library will populate an object containing all the dynamicTags that are available in the current context:

```javascript
/* window context */
dynamicTags: {
    tags: {
        "tag-id": {
            $elem: $elem, //HTMLSpanElement element that contains the dynamic tag structure
            $target: $elem, //HTMLSpanElement element that contains the target value to change
            isReplaced: true, //boolean that tells whether this value has been updated at least once
            isWaiting: false, //boolean that tells whether this tag is waiting for a substitution, and has its spinner visible
            replace: function, //callback that accepts any value that can be printed as string to substitute into the DOM
            wait: function, //callback that activates the spinner for this dynamicTag
            stop: function, //callback that stops the spinner for this dynamicTag
            
        }
    }
} 
```

### Source code
<a href="https://www.drupal.org/project/ckeditor5_tags">Drupal Module</a>

### Say Thanks!

If you find this module helpful, and you want to contribute to my work, or allow me to spend more of my free time on this, feel free to show your appreciation!
Even the smallest donation is of great encouragment and is much appreciated!

<a href="https://buymeacoffee.com/devicious.dev"><img src="https://www.drupal.org/files/project-images/buymeacoffee.png" /></a>