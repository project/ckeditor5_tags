/**
 * @file defines TagsInsertCommand, which is executed when the plugin
 * toolbar button is pressed.
 */

import { Command } from "ckeditor5/src/core";

export default class TagsInsertCommand extends Command {
  execute() {
    const { model } = this.editor;

    model.change((writer) => {
      // Insert <writer>*</writer> at the current selection position
      // in a way that will result in creating a valid model structure.
      model.insertContent(addTag(writer));
    });
  }

  refresh() {
    const { model } = this.editor;
    const { selection } = model.document;

    // Determine if the cursor (selection) is in a position where adding a
    // component is permitted. This is based on the schema of the model(s)
    // currently containing the cursor.
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      "dynamicTag",
    );

    // If the cursor is not in a location where content can be added, return
    // null so the addition doesn't happen.
    this.isEnabled = allowedIn !== null;
  }
}

function addTag(writer) {
  // Create instances of the three registered elements
  const tag = writer.createElement("dynamicTag");
  const id = writer.createElement("tagId");
  const label = writer.createElement("tagLabel");

  writer.appendText("tag-id", id);
  writer.appendText("Label", label);

  writer.append(id, tag);
  writer.append(label, tag);

  // Return the element to be added to the editor.
  return tag;
}
