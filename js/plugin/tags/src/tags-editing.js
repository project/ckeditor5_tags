import { Plugin } from "ckeditor5/src/core";
import { toWidget, toWidgetEditable } from "ckeditor5/src/widget";
import { Widget } from "ckeditor5/src/widget";
import TagsInsertCommand from "./tags-insert-command";

/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 *
 * This file has the logic for defining the plugin model, and for how it is
 * converted to standard DOM markup.
 */
export default class TagsEditing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();
    this.editor.commands.add("insertTag", new TagsInsertCommand(this.editor));
  }

  /*
   * This registers the structure that will be seen by CKEditor 5
   *
   * The logic in _defineConverters() will determine how this is converted to
   * markup.
   */
  _defineSchema() {
    // Schemas are registered via the central `editor` object.
    const schema = this.editor.model.schema;

    schema.register("dynamicTag", {
      isInline: true,
      isObject: true,
      isSelectable: true,
      allowIn: [
        "$container",
        "$documentFragment",
        "$htmlList",
        "$inlineObject",
        "$listItem",
        "$marker",
        "$root",
        "$text",
        "blockQuote",
        "caption",
        "codeBlock",
        "heading1",
        "heading2",
        "heading3",
        "heading4",
        "heading5",
        "heading6",
        "htmlArticle",
        "htmlAside",
        "htmlBlockquote",
        "htmlButton",
        "htmlCenter",
        "htmlDiv",
        "htmlDivParagraph",
        "htmlH1",
        "htmlH2",
        "htmlH3",
        "htmlH4",
        "htmlH5",
        "htmlH6",
        "htmlLegend",
        "htmlSection",
        "htmlSummary",
        "htmlTable",
        "htmlTbody",
        "htmlTd",
        "htmlTextarea",
        "htmlTfoot",
        "htmlTh",
        "htmlThead",
        "htmlTr",
        "paragraph",
        "rawHtml",
        "table",
        "tableCell",
        "tableColumn",
        "tableColumnGroup",
        "tableRow",
      ],
      allowAttributes: "dataDynamicTag",
    });

    schema.register("tagId", {
      isLimit: true,
      allowIn: "dynamicTag",
      isInline: true,
      isObject: true,
      allowChildren: ["$text"],
      allowContentOf: "$block",
      allowAttributes: "",
    });

    schema.register("tagLabel", {
      isLimit: true,
      allowIn: "dynamicTag",
      isInline: true,
      isObject: true,
      allowChildren: ["$text"],
      allowContentOf: "$block",
      allowAttributes: "",
    });

    schema.addChildCheck((context, childDefinition) => {
      // Disallow self injections.
      if (
        (context.endsWith("tagId") || context.endsWith("tagLabel")) &&
        childDefinition.name === "dynamicTag"
      ) {
        return false;
      }
    });

    schema.addAttributeCheck((context, attributeName) => {
      if (context._items.some((item) => item.name === "tagId")) {
        return false;
      }
    });

    const doc = this.editor.model.document;
    doc.registerPostFixer((writer) => {
      const changes = doc.differ.getChanges();
      for (const entry of changes) {
        if (entry.type === "insert" && entry.position.parent.name === "tagId") {
          const value = entry.position.textNode?.data;
          if (value && !!value.match(/[^a-zA-Z0-9-_]/gi)) {
            writer.remove(entry.position.textNode);
            writer.insertText(
              value.replaceAll(/[^a-zA-Z0-9-_]/gim, ""),
              entry.position.parent,
            );

            return true;
          }
        }
      }

      return false;
    });
  }

  /**
   * Converters determine how CKEditor 5 models are converted into markup and
   * vice-versa.
   */
  _defineConverters() {
    // Converters are registered via the central editor object.
    const { conversion } = this.editor;

    // Upcast Converters: determine how existing HTML is interpreted by the
    // editor. These trigger when an editor instance loads.
    //
    // If <section class="dynamic-tag"> is present in the existing markup
    // processed by CKEditor, then CKEditor recognizes and loads it as a
    // <dynamicTag> model.
    conversion.for("upcast").elementToElement({
      model: "dynamicTag",
      view: {
        name: "span",
        classes: "dynamic-tag",
      },
    });

    conversion.for("upcast").elementToElement({
      model: "tagId",
      view: {
        name: "code",
        classes: "tag-id",
      },
    });

    conversion.for("upcast").elementToElement({
      model: "tagLabel",
      view: {
        name: "span",
        classes: "tag-label",
      },
    });

    // Data Downcast Converters: converts stored model data into HTML.
    // These trigger when content is saved.
    conversion.for("dataDowncast").elementToElement({
      model: "dynamicTag",
      view: (modelElement, { writer: viewWriter }) => {
        return viewWriter.createContainerElement("span", {
          class: "dynamic-tag",
          "dynamic-tag": modelElement.getChild(0)?.getChild(0)?.data || "",
        });
      },
    });


    conversion.for("dataDowncast").elementToElement({
      model: "tagId",
      view: {
        name: "code",
        classes: "tag-id",
      },
    });

    conversion.for("dataDowncast").elementToElement({
      model: "tagLabel",
      view: {
        name: "span",
        classes: "tag-label",
      },
    });

    // Editing Downcast Converters. These render the content to the user for
    // editing, i.e. this determines what gets seen in the editor. These trigger
    // after the Data Upcast Converters, and are re-triggered any time there
    // are changes to any of the models' properties.
    //
    conversion.for("editingDowncast").elementToElement({
      model: "dynamicTag",
      view: (modelElement, { writer: viewWriter }) => {
        const section = viewWriter.createContainerElement("span", {
          class: "dynamic-tag",
        });

        return toWidget(section, viewWriter, { label: "Dynamic Tag" });
      },
    });

    conversion.for("editingDowncast").elementToElement({
      model: "tagId",
      view: (modelElement, { writer: viewWriter }) => {
        const span = viewWriter.createEditableElement("code", {
          class: "tag-id",
        });
        return toWidgetEditable(span, viewWriter);
      },
    });

    conversion.for("editingDowncast").elementToElement({
      model: "tagLabel",
      view: (modelElement, { writer: viewWriter }) => {
        const label = viewWriter.createEditableElement("span", {
          class: "tag-label",
        });
        return toWidgetEditable(label, viewWriter);
      },
    });
  }
}
