/**
 * @file registers the plugin toolbar button and binds functionality to it.
 */

import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import icon from '../../../../icons/tags.svg';

export default class TagsUI extends Plugin {
  init() {
    const editor = this.editor;

    // This will register the plugin toolbar button.
    editor.ui.componentFactory.add('tags', (locale) => {
      const command = editor.commands.get('insertTag');
      const buttonView = new ButtonView(locale);

      // Create the toolbar button.
      buttonView.set({
        label: editor.t('Dynamic Tags'),
        icon,
        tooltip: true,
      });

      // Bind the state of the button to the command.
      buttonView.bind('isOn', 'isEnabled').to(command, 'value', 'isEnabled');

      // Execute the command when the button is clicked (executed).
      this.listenTo(buttonView, 'execute', () =>
        editor.execute('insertTag'),
      );

      return buttonView;
    });
  }
}
